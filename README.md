# sining

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A multimedia social media.

TODO: Fill out this long description.

## Table of Contents

- [sining](#sining)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
```

## Usage

```
```

## Maintainers

[@harudagondi](https://github.com/harudagondi)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

AGPL-3-or-later © 2020 Gio Genre De Asis
